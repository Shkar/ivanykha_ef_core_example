﻿using System.Linq;
using MyPlannerModel;

namespace Methods
{
    public class StatusMethods
    {
        public static void InitialAddNewEventStatus()
        {
            /*using (AppModelContext _context = new AppModelContext())
            {
                _context.EventStatus.AddRange(
                   new EventStatus {Name = "New"},
                                new EventStatus { Name = "In Progress" },
                                new EventStatus { Name = "Done" },
                                new EventStatus { Name = "Canceled" }
                    );
                _context.SaveChanges();
            }*/
        }

        public static EventStatus GetEventStatusDb(int id)
        {
            using (AppModelContext _context = new AppModelContext())
                {return _context.EventStatus.SingleOrDefault(s => s.Id == id);}
        }

        public static EventStatus GetEventStatusDb(string name)
        {
            using (AppModelContext _context = new AppModelContext())
            { return _context.EventStatus.SingleOrDefault(s => s.Name == name); }
        }
    }
}
