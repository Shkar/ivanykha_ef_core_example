﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyPlannerModel;

namespace Methods
{
    public class ListMethods
    {
        public static List<ListPlanned> GetListPlannedWithEventsDb()
        {
            using (AppModelContext _context = new AppModelContext())
            {
                return _context.ListPlanned
                    .Include(l =>l.EventPlanned)
                    .ToList();
            }
        }

        public static List<ListPlanned> GetListPlannedWithEventsDb(int id)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                var ListPlanned= _context.ListPlanned.Find(id);
                return _context.ListPlanned.Include(l => l.EventPlanned).ToList();

            }
        }

        public static ListPlanned GetListPlannedWithEventsDb(string listname)
        {
            using (AppModelContext _context = new AppModelContext())
            {return _context.ListPlanned
                .Where(l => l.Name==listname)
                .Include(l => l.EventPlanned)
                .FirstOrDefault();
            }
        }

        public static ListPlanned GetListPlannedDb(int id)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                return _context.ListPlanned
                    .Where(l => l.Id == id)
                    .Include(l => l.EventPlanned)
                    .FirstOrDefault();

            }
        }

        public static ListPlanned GetListPlannedDb(string name)
        {
            using (AppModelContext _context = new AppModelContext())
            { return _context.ListPlanned
                .Where(l => l.Name == name)
                .Include(l => l.EventPlanned)
                .FirstOrDefault(); }
        }

        public static void UpdateListPlannedDb(ListPlanned listPlanned)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                _context.ListPlanned.Update(listPlanned);
                _context.SaveChanges();
            }
        }

        public static void AddListPlannedDb(ListPlanned listPlanned)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                _context.ListPlanned.Add(listPlanned);
                _context.SaveChanges();
            }
        }

        public static void AddListPlannedDb(List<ListPlanned> listPlanned)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                _context.ListPlanned.AddRange(listPlanned);
                _context.SaveChanges();
            }
        }

        public static void DeleteListPlannedDb(int id)
        {
            using (AppModelContext _context = new AppModelContext())
            {
                var eventToDelete = _context.ListPlanned
                    .Where(l => l.Id == id)
                    .Include(l => l.EventPlanned)
                    .SingleOrDefault();

                _context.ListPlanned.Remove(eventToDelete);
                _context.SaveChanges();
            }
        }
    }
}
