﻿using System.Collections.Generic;

namespace MyPlannerModel
{
    public class EventStatus
    {
        public EventStatus()
            {
        //EventPlanned = new List<EventPlanned>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        //public int EventPlannedId { get; set; }
        public List<EventPlanned> EventPlanned { get; set; }
       
    }
}
