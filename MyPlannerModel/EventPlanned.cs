﻿using System;

namespace MyPlannerModel
{
    public class EventPlanned
    {
        public EventPlanned()
        {
            //ListPlanned = new ListPlanned();
            //EventStatus = new EventStatus();
        }
        
        public int Id { get; set; }

        private string _name;
        public string Name { get=>_name;
            set
            {
                if (value.Length>=20)
                {
                   throw  new ArgumentException("Name should be less than 20 symbols");
                }
                _name = value;
            }
        }
        public string Description { get; set; }
        public DateTime? DateTodo { get; set; }

        public int EventStatusId { get; set; }
        public virtual EventStatus EventStatus { get; set; }

        public int? ListPlannedId { get; set; }
        public virtual  ListPlanned ListPlanned { get; set; }
    }   
}
