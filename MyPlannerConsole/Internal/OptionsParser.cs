﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;

namespace MyPlannerConsole
{
    internal class OptionsParser
    {
        private static readonly Lazy<Parser> LazyInstance = new Lazy<Parser>(() => new Parser(settings =>
        {
            settings.IgnoreUnknownArguments = false;
        }));

        public static Parser Instance
        {
            get { return LazyInstance.Value; }
        }
    }
}