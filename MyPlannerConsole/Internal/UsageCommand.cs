﻿using System;
using System.Linq;
using MyPlannerConsole;

namespace MyPlannerConsole
{
    class UsageCommand : ICommand
    {
        public const string Key = "help";
        private readonly ICommand[] _allcommands;
        public UsageCommand(ICommand[] allcommands)
        {
            _allcommands = allcommands;
        }

        public string Usage()
            {return string.Format("{0}", Key);}

        public string Description()
        {return "list of commands and usage";}

        public int Run(string[] args)
        {
            foreach (var command in _allcommands.Union(_allcommands))
            {
                Console.WriteLine(command.Usage());
                Console.WriteLine(command.Description());
            }

            return ExitCode.Success;
        }

    }
}
