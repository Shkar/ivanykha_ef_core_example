﻿using System;

namespace MyPlannerConsole
{
    class ExitCode
    {
        public const int Success = 0;
        public const int Error = 1;
    }
    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("Hello");
            try
                {return Run(args);}
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }

        private static int Run(string[] args)
           {return new CommandsProcessor().Process(args);}
    }
}
