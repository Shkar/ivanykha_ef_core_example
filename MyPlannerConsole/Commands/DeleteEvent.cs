﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class DeleteEventOptions : OptionsBase
    {
        [Option('i', "id", Required = true)] public int Id { get; set; }
        
    }

    internal class DeleteEvent : ICommand
    {
        public const string Key = "delete-event";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new DeleteEventOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Deletes existing Item in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new DeleteEventOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Delete(options, new AppModelContext());
            }
                
            return ExitCode.Error;
        }

        private int Delete(DeleteEventOptions options, AppModelContext context)
        {
            try
            {
                new EventMethods(context).DeletePlannedEventByIdDb(options.Id);
                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

