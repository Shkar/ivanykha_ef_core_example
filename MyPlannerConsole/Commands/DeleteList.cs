﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class DeleteListOptions : OptionsBase
    {
        [Option('i', "id", Required = true, HelpText = "Id of List. that should be deleted")]
        public int Id { get; set; }
    }

    internal class DeleteList : ICommand
    {
        public const string Key = "delete-list";

        public string Usage()
            {return string.Format("{0}{1}", Key, new DeleteListOptions().GetUsageForUsageList());}

        public string Description()
            {return string.Format("Description: Command \"{0}\" Deletes List in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new DeleteListOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Delete(options);
            }
            return ExitCode.Error;
        }

        private int Delete(DeleteListOptions options)
        {
            try
            {
                var listPlanned = ListMethods.GetListPlannedDb(options.Id);
                
                if (listPlanned.EventPlanned.Count > 0)
                {
                    Console.WriteLine();
                    Console.WriteLine(String.Format(("Are You sure, you want to delete List \"{0}\"?" +
                                      "\nIt contains {1} items. Items won't be deleted" +
                                      "\nPlease type \"y\" or \"yes\" for confirmation, Please type \"n\" or \"nos\" for canceling"), listPlanned.Name, listPlanned.EventPlanned.Count));

                    bool stop = false;

                    do
                    {
                        string inputstring = Console.ReadLine();
                        if (inputstring.ToLower() == "yes" || inputstring.ToLower() == "y")
                        {
                            ListMethods.DeleteListPlannedDb(options.Id);
                            Console.WriteLine("List was deleted");
                            stop = true;
                        }
                        else if (inputstring.ToLower() == "no" || inputstring.ToLower() == "n")
                        {
                            return ExitCode.Success;
                            stop = true;
                        }
                    } while (stop);
                }
                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

