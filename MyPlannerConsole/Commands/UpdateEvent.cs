﻿using System;
using CommandLine;
using Methods;
using MyPlannerModel;

namespace MyPlannerConsole
{
    internal class UpdateEventOptions : OptionsBase
    {
        [Option('i', "id", Required=true, HelpText = "Id of Event that should be modified")] public int Id { get; set; }
        [Option('n', "name", Required = false, HelpText = "Name of Event. Please enclose with quotation marks (\" \") if name is not a single word")]
        public string Name { get; set; }

        [Option('d', "description", Required = false, HelpText = "Description of event")]
        public string Description { get; set; }

        [Option('s', "Status", Required = false, HelpText = "Status of event")]
        public string Status { get; set; }

        [Option('l', "list", Required = false, HelpText = "List of event")]
        public string List { get; set; }

        [Option("date", Required = false, HelpText = "Scheduled Date to do.")]
        public DateTime? DateToDo { get; set; }
    }

    internal class UpdateEvent : ICommand
    {
        public const string Key = "update-event";


        public string Usage()
        {
            return string.Format("{0}{1}", Key, new UpdateEventOptions().GetUsageForUsageList());
        }

        public string Description()
        { return string.Format("Description: Command \"{0}\" Updates existing Item in \"My Planner\".", Key); }

        public int Run(string[] arguments)
        {
            var options = new UpdateEventOptions();
            if (OptionsParser.Instance.ParseArguments(arguments, options))
            {
                return Update(options, new AppModelContext());
            }
            return ExitCode.Error;
        }

        private int Update(UpdateEventOptions options, AppModelContext context)
        {
            try
            {
                //AppModelContext context = new  AppModelContext();
                EventPlanned eventPlanned = new EventMethods(context).GetPlannedEventDb(options.Id);

                if (options.Name != null)
                {
                    eventPlanned.Name = options.Name;
                }

                if (options.Description != null)
                {
                    eventPlanned.Description = options.Description;
                }

                if (options.DateToDo != null)
                {
                    eventPlanned.DateTodo = options.DateToDo;
                }

                if (options.Status != null)
                {
                    eventPlanned.EventStatus = Int32.TryParse(options.Status, out int statusId) 
                        ? StatusMethods.GetEventStatusDb(statusId) : StatusMethods.GetEventStatusDb(options.Status);
                }

                if (options.List != null)
                {
                    if (options.List == "delete")
                    {
                        eventPlanned.ListPlanned = null;
                        eventPlanned.ListPlannedId = null;
                    }
                    
                    else if (Int32.TryParse(options.List, out int listId))
                    {
                        eventPlanned.ListPlanned = ListMethods.GetListPlannedDb(listId);
                    }
                }

                new EventMethods(context).UpdatePlannedEventDb(eventPlanned);
                Console.WriteLine("Update was successful");
                return ExitCode.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return ExitCode.Error;
            }
        }
    }
}

