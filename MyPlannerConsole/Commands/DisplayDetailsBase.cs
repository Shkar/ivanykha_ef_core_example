﻿using System;
using System.Collections.Generic;
using System.Text;
using MyPlannerModel;

namespace MyPlannerConsole.Commands
{
    public class DisplayDetailsBase
    {
       public void DisplayDetails(List<EventPlanned> eventPlanned, string description)
       {
           if (eventPlanned.Count > 0)
           {
               Console.WriteLine();
               Console.WriteLine("Planned events that match {0}:", description);
               Console.WriteLine();
               foreach (var eventitem in eventPlanned)
               {
                   string listName;
                   if (eventitem.ListPlanned != null)
                   {
                       listName = (eventitem.ListPlanned.Name);
                   }
                   else
                   {
                       listName = "";
                   }

                   Console.WriteLine("Event Id {0} \nEvent name: {1} \nDescription: {2} \nDate to Do: {3:d} \nStatus: {4} \nList: {5}",
                       eventitem.Id, eventitem.Name, eventitem.Description, eventitem.DateTodo, eventitem.EventStatus.Name, listName);
                   Console.WriteLine();
               }
           }
           else
           {
               Console.WriteLine();
               Console.WriteLine("Nothing to display");
               Console.WriteLine();
           }


        }

       public void DisplayDetails(EventPlanned eventPlanned, string description)
       {
           if (eventPlanned!=null)
           {
               Console.WriteLine();
               Console.WriteLine("Planned events that match {0}:", description);
               Console.WriteLine();
        
                   string listName;
                   if (eventPlanned.ListPlanned != null)
                   {
                       listName = (eventPlanned.ListPlanned.Name);
                   }
                   else
                   {
                       listName = "";
                   }

                   Console.WriteLine("Event Id {0} \nEvent name: {1} \nDescription: {2} \nDate to Do: {3:d} \nStatus: {4} \nList: {5}",
                       eventPlanned.Id, eventPlanned.Name, eventPlanned.Description, eventPlanned.DateTodo, eventPlanned.EventStatus.Name, listName);
                   Console.WriteLine();

           }
           else
           {
               Console.WriteLine();
               Console.WriteLine("Nothing to display");
               Console.WriteLine();
           }


       }
    }
}
